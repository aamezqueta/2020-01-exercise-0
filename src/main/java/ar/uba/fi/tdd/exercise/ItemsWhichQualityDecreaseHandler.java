package ar.uba.fi.tdd.exercise;

public abstract class ItemsWhichQualityDecreaseHandler extends ItemHandler {

    public ItemsWhichQualityDecreaseHandler (int sellIn,int quality, String itemName)
            throws QualityOutOfRangeException{
        super(sellIn,quality,itemName);
    }

    protected void decreaseQuality(){
        if( this.itsItem.quality > MIN_QUALITY ){
            this.itsItem.quality--;
        }
    }
}

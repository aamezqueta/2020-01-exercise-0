package ar.uba.fi.tdd.exercise;

public class ItemsHandlersFactory {

    private static final int SULFURAS_QUALITY = 80;
    private static final int LEGENDARY_ITEM_SELL_IN = 0;
    private static final String SULFURAS_NAME = "Sulfuras, Hand of Ragnaros";
    private static final String AGED_BRIE = "Aged Brie";
    private static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";



    public static SulfurasHandler createSulfurasHandler() throws QualityOutOfRangeException {
        return new SulfurasHandler(LEGENDARY_ITEM_SELL_IN,SULFURAS_QUALITY,SULFURAS_NAME);
    }

    public static AgedBrieHandler createAgedBrieHandler(int quality, int sellIn)
            throws QualityOutOfRangeException {

        return new AgedBrieHandler(sellIn,quality,AGED_BRIE);
    }

    public static BackstagePassesHandler createBackstagePassesHandler(int quality, int sellIn)
            throws QualityOutOfRangeException {

        return new BackstagePassesHandler(sellIn,quality,BACKSTAGE_PASSES);
    }

    public static ConjuredItemHandler createConjuredItemHandler(String itemName, int quality, int sellIn)
            throws QualityOutOfRangeException {

        return new ConjuredItemHandler(sellIn,quality,itemName);
    }

    public static NormalItemHandler createNormalItemHandler(String itemName, int quality, int sellIn)
            throws QualityOutOfRangeException {

        return new NormalItemHandler(sellIn,quality,itemName);
    }




}

package ar.uba.fi.tdd.exercise;

public abstract class LegendaryItemsHandler extends ItemHandler {

    public LegendaryItemsHandler(int sellIn,int quality, String itemName)
            throws QualityOutOfRangeException{

        super(sellIn, quality, itemName);
    }

    @Override
    public void handleQuality(){
        return;
    }

    @Override
    public void reduceSellIn(){
        return;
    }

    @Override
    public void checkQualityAferSDeadline(){
        return;
    }
}

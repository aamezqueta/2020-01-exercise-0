package ar.uba.fi.tdd.exercise;

public class SulfurasHandler extends LegendaryItemsHandler{

    public SulfurasHandler(int sellIn,int quality, String itemName)
            throws QualityOutOfRangeException{
        super(sellIn, 0, itemName);
        this.itsItem.quality = quality;
    }
}


package ar.uba.fi.tdd.exercise;

class GildedRose {

    private ItemHandler[] handlers;

    public GildedRose(ItemHandler[] _handlers) {
        handlers = _handlers;
    }

    public Item getItem(int pos){
        return this.handlers[pos].getItem();
    }


    public void updateQuality() {

        for (int i = 0; i < handlers.length; i++) {

            this.handlers[i].handleQuality();
            this.handlers[i].reduceSellIn();
            this.handlers[i].checkQualityAferSDeadline();
        }
    }
}

package ar.uba.fi.tdd.exercise;

public class AgedBrieHandler extends ItemsWhichQualityIncreaseHandler{

    public AgedBrieHandler(int sellIn, int quality,String itemName)
            throws QualityOutOfRangeException{
        super(sellIn,quality,itemName);
    }

    @Override
    public void handleQuality(){
        this.increaseQuality();
    }
}

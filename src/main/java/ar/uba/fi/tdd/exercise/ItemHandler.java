package ar.uba.fi.tdd.exercise;

public abstract class ItemHandler {

    protected Item itsItem;
    public static final int MAX_QUALITY = 50;
    public static final int MIN_QUALITY = 0;
    protected static final int DEADLINE = 0;
    private final String ERROR_MESSAGE = "Error: quality out of range, must be less or equal to 50 and greater or equal to 0";

    public ItemHandler(int sellIn,int quality, String itemName) throws QualityOutOfRangeException{

        if ( quality < MIN_QUALITY || quality > MAX_QUALITY ){
            throw new QualityOutOfRangeException(ERROR_MESSAGE);
        }
        this.itsItem = new Item(itemName,sellIn,quality);

    }

    public abstract void handleQuality();


    public void reduceSellIn(){
        this.itsItem.sellIn--;
    }

    public void checkQualityAferSDeadline(){
        if( this.itsItem.sellIn < DEADLINE )
            this.handleQuality();
    }

    public Item getItem(){
        return this.itsItem;
    }

}



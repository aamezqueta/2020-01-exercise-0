package ar.uba.fi.tdd.exercise;

public class QualityOutOfRangeException extends Exception {

    public QualityOutOfRangeException(String errorMessage){
        super(errorMessage);
    }
}

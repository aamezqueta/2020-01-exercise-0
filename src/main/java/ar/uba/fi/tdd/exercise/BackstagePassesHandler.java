package ar.uba.fi.tdd.exercise;

public class BackstagePassesHandler extends ItemsWhichQualityIncreaseHandler {

    public final static int TEN_DAYS_TO_DEADLINE = 10;
    public final static int FIVE_DAYS_TO_DEADLINE = 5;

    public BackstagePassesHandler(int sellIn, int quatlity,String itemName)
            throws QualityOutOfRangeException{

        super(sellIn,quatlity, itemName);
    }

    @Override
    public void handleQuality(){

        this.increaseQuality();

        if (this.itsItem.sellIn <= TEN_DAYS_TO_DEADLINE) {
            this.increaseQuality();
        }

        if (this.itsItem.sellIn <= FIVE_DAYS_TO_DEADLINE) {
            this.increaseQuality();
        }
    }

    @Override
    public void checkQualityAferSDeadline(){
        if ( this.itsItem.sellIn < DEADLINE )
            this.itsItem.quality = MIN_QUALITY;
    }
}

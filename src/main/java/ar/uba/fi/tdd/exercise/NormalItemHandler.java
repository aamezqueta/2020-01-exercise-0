package ar.uba.fi.tdd.exercise;

public class NormalItemHandler extends ItemsWhichQualityDecreaseHandler {

    public NormalItemHandler(int sellIn, int quality,String name) throws QualityOutOfRangeException{
        super(sellIn,quality,name);
    }

    @Override
    public void handleQuality(){
        this.decreaseQuality();
    }
}

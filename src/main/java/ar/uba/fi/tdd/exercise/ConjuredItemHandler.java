package ar.uba.fi.tdd.exercise;

public class ConjuredItemHandler extends ItemsWhichQualityDecreaseHandler {

        private final int N_ITERATIONS = 2;

        public ConjuredItemHandler(int sellIn,int quality, String itemName) throws QualityOutOfRangeException{
            super(sellIn,quality,itemName);
        }

        @Override
        public void handleQuality(){
            for( int i = 0; i < N_ITERATIONS; i++ ){
                this.decreaseQuality();
            }
        }
}

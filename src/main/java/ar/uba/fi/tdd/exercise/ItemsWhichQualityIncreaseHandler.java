package ar.uba.fi.tdd.exercise;

public abstract class ItemsWhichQualityIncreaseHandler extends ItemHandler {

    public ItemsWhichQualityIncreaseHandler (int sellIn,int quality, String itemName)
            throws QualityOutOfRangeException{
         super(sellIn,quality,itemName);
    }

    protected void increaseQuality(){
        if (this.itsItem.quality < MAX_QUALITY) {
            this.itsItem.quality++;
        }
    }
}

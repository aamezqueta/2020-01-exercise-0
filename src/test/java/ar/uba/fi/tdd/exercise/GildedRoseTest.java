package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;



@SpringBootTest
class GildedRoseTest {

	private final static String APPLE = "Apple";
	private final static String SWORD = "Sword";
	private static final int SULFURAS_QUALITY = 80;
	private static final int SULFURAS_SELL_IN = 0;

	private final int FIRST_ITEM = 0;
	private final int SECOND_ITEM = 1;
	private final int THIRD_ITEM = 2;
	private final int FOURTH_ITEM = 3;
	private final int FIFTH_ITEM = 4;

	private final int QUALITY_IS_TWENTY = 20;

	private final int SELL_IN_FIFTHTEEN_DAYS = 15;
	private final int SELL_IN_TEN_DAYS = 10;
	private final int SELL_IN_FIVE_DAYS = 5;
	private final int A_DAY_AFTER_THE_SELL_IN = -1;
	private final int A_DAY = 1;
	private final int ONE = 1;
	private final int TWO = 2;
	private final int THREE = 3;
	private final int FOUR = 4;
	private final String ERROR_MESSAGE = "Error: quality out of range, must be less or equal to 50 and greater or equal to 0";

	@Test
	public void testItemName() throws QualityOutOfRangeException{

			ItemHandler[] handlers = new ItemHandler[] {
					ItemsHandlersFactory.createNormalItemHandler(APPLE,QUALITY_IS_TWENTY,SELL_IN_FIFTHTEEN_DAYS)
			};
			GildedRose app = new GildedRose(handlers);

			app.updateQuality();
			assertThat(app.getItem(FIRST_ITEM).Name).isEqualTo(APPLE);
	}

	@Test
	public void testSellInDecreaseForEveryItemExceptSulfuras() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createNormalItemHandler(APPLE,QUALITY_IS_TWENTY,SELL_IN_FIFTHTEEN_DAYS),
				ItemsHandlersFactory.createAgedBrieHandler(QUALITY_IS_TWENTY,SELL_IN_FIFTHTEEN_DAYS),
				ItemsHandlersFactory.createBackstagePassesHandler(QUALITY_IS_TWENTY, SELL_IN_FIFTHTEEN_DAYS),
				ItemsHandlersFactory.createSulfurasHandler(),
				ItemsHandlersFactory.createConjuredItemHandler(SWORD,QUALITY_IS_TWENTY,SELL_IN_FIFTHTEEN_DAYS)
		};
		GildedRose app = new GildedRose(handlers);

		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).sellIn).isEqualTo(SELL_IN_FIFTHTEEN_DAYS - A_DAY);
		assertThat(app.getItem(SECOND_ITEM).sellIn).isEqualTo(SELL_IN_FIFTHTEEN_DAYS - A_DAY);
		assertThat(app.getItem(THIRD_ITEM).sellIn).isEqualTo(SELL_IN_FIFTHTEEN_DAYS - A_DAY);
		assertThat(app.getItem(FOURTH_ITEM).sellIn).isEqualTo(SULFURAS_SELL_IN);
		assertThat(app.getItem(FIFTH_ITEM).sellIn).isEqualTo(SELL_IN_FIFTHTEEN_DAYS - A_DAY);
	}

	@Test
	public void testQualityDecreaseNormalItemBeforeSellIn() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createNormalItemHandler(APPLE,QUALITY_IS_TWENTY,SELL_IN_FIFTHTEEN_DAYS)
		};
		GildedRose app = new GildedRose(handlers);

		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY - ONE);
	}

	@Test
	public void testQualityDecreaseTwiceNormalItemAfterSellIn() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createNormalItemHandler(APPLE,QUALITY_IS_TWENTY,A_DAY_AFTER_THE_SELL_IN)
		};
		GildedRose app = new GildedRose(handlers);

		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY - TWO);
	}

	@Test
	public void testQualityIncreaseForAgedBrieTheOlderItGets() throws QualityOutOfRangeException {

		ItemHandler[] handlers = new ItemHandler[] { ItemsHandlersFactory.createAgedBrieHandler(QUALITY_IS_TWENTY,SELL_IN_FIFTHTEEN_DAYS) };
		GildedRose app = new GildedRose(handlers);

		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY + ONE);
	}

	@Test
	public void testQualityIncreaseByTwoForAgedBrieTheOlderItGetsAfterSellIn() throws QualityOutOfRangeException {

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createAgedBrieHandler(QUALITY_IS_TWENTY,A_DAY_AFTER_THE_SELL_IN)
		};
		GildedRose app = new GildedRose(handlers);

		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY + TWO);
	}

	@Test
	public void testQualityDoesntChangeForSulfuras() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] { ItemsHandlersFactory.createSulfurasHandler() };
		GildedRose app = new GildedRose(handlers);


		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(SULFURAS_QUALITY);
	}

	@Test
	public void testQualityIncreaseForBackStagePassesButDropsToCeroAferConcert() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createBackstagePassesHandler(QUALITY_IS_TWENTY, SELL_IN_FIFTHTEEN_DAYS),
				ItemsHandlersFactory.createBackstagePassesHandler(QUALITY_IS_TWENTY, SELL_IN_TEN_DAYS),
				ItemsHandlersFactory.createBackstagePassesHandler(QUALITY_IS_TWENTY, SELL_IN_FIVE_DAYS),
				ItemsHandlersFactory.createBackstagePassesHandler(QUALITY_IS_TWENTY, A_DAY_AFTER_THE_SELL_IN)};

		GildedRose app = new GildedRose(handlers);
		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY + ONE);
		assertThat(app.getItem(SECOND_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY + TWO);
		assertThat(app.getItem(THIRD_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY + THREE);
		assertThat(app.getItem(FOURTH_ITEM).quality).isEqualTo(ItemHandler.MIN_QUALITY);
	}

	@Test
	public void testQualityNeverSurpassFiftyForAgedBrieAndBackstagePasses() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createBackstagePassesHandler(ItemHandler.MAX_QUALITY, SELL_IN_FIFTHTEEN_DAYS),
				ItemsHandlersFactory.createBackstagePassesHandler(ItemHandler.MAX_QUALITY - ONE,SELL_IN_TEN_DAYS),
				ItemsHandlersFactory.createBackstagePassesHandler(ItemHandler.MAX_QUALITY - ONE, SELL_IN_FIVE_DAYS),
				ItemsHandlersFactory.createAgedBrieHandler(ItemHandler.MAX_QUALITY - ONE,SELL_IN_TEN_DAYS),
				ItemsHandlersFactory.createAgedBrieHandler(ItemHandler.MAX_QUALITY - ONE,A_DAY_AFTER_THE_SELL_IN)};
		GildedRose app = new GildedRose(handlers);


		app.updateQuality();
		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(ItemHandler.MAX_QUALITY);
		assertThat(app.getItem(SECOND_ITEM).quality).isEqualTo(ItemHandler.MAX_QUALITY);
		assertThat(app.getItem(THIRD_ITEM).quality).isEqualTo(ItemHandler.MAX_QUALITY);
		assertThat(app.getItem(FOURTH_ITEM).quality).isEqualTo(ItemHandler.MAX_QUALITY);
		assertThat(app.getItem(FIFTH_ITEM).quality).isEqualTo(ItemHandler.MAX_QUALITY);
	}

	@Test
	public void testQualityNeverDropsLessthanZeroForNormalItems() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createNormalItemHandler(APPLE,ItemHandler.MIN_QUALITY + ONE,SELL_IN_FIFTHTEEN_DAYS) };
		GildedRose app = new GildedRose(handlers);

		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(ItemHandler.MIN_QUALITY);
		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(ItemHandler.MIN_QUALITY);
		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(ItemHandler.MIN_QUALITY);
	}

	@Test
	public void testQualityNeverIsLessThanZeroForAnyItem(){
		try {
			ItemsHandlersFactory.createNormalItemHandler(APPLE, ItemHandler.MIN_QUALITY - ONE, SELL_IN_FIFTHTEEN_DAYS);
			Assertions.fail("Error: QualityOutOfRangeException quality exception expected");

		}
		catch ( QualityOutOfRangeException  e){
			assertThat(e)
					.isInstanceOf(QualityOutOfRangeException.class)
					.hasMessage(ERROR_MESSAGE);
		}

		try {
			ItemsHandlersFactory.createAgedBrieHandler(ItemHandler.MIN_QUALITY - ONE, SELL_IN_FIFTHTEEN_DAYS);
			Assertions.fail("Error: QualityOutOfRangeException quality exception expected");

		}
		catch ( QualityOutOfRangeException  e){
			assertThat(e)
					.isInstanceOf(QualityOutOfRangeException.class)
					.hasMessage(ERROR_MESSAGE);
		}

		try {
			ItemsHandlersFactory.createBackstagePassesHandler(ItemHandler.MIN_QUALITY - ONE, SELL_IN_FIFTHTEEN_DAYS);
			Assertions.fail("Error: QualityOutOfRangeException quality exception expected");

		}
		catch ( QualityOutOfRangeException  e){
			assertThat(e)
					.isInstanceOf(QualityOutOfRangeException.class)
					.hasMessage(ERROR_MESSAGE);
		}

		try {
			ItemsHandlersFactory.createConjuredItemHandler(SWORD, ItemHandler.MIN_QUALITY - ONE,SELL_IN_FIFTHTEEN_DAYS);
			Assertions.fail("Error: QualityOutOfRangeException quality exception expected");
		}
		catch ( QualityOutOfRangeException  e){
			assertThat(e)
					.isInstanceOf(QualityOutOfRangeException.class)
					.hasMessage(ERROR_MESSAGE);
		}
	}

	@Test
	public void testQualityNeverIsGreaterThanFiftyForAnyItemExceptsSulfuras(){
		try {
			ItemsHandlersFactory.createNormalItemHandler(APPLE, ItemHandler.MAX_QUALITY + ONE, SELL_IN_FIFTHTEEN_DAYS);
			Assertions.fail("Error: QualityOutOfRangeException quality exception expected");
		}
		catch ( QualityOutOfRangeException  e){
			assertThat(e)
					.isInstanceOf(QualityOutOfRangeException.class)
					.hasMessage(ERROR_MESSAGE);
		}

		try {
			ItemsHandlersFactory.createAgedBrieHandler(ItemHandler.MAX_QUALITY + ONE, SELL_IN_FIFTHTEEN_DAYS);
			Assertions.fail("Error: QualityOutOfRangeException quality exception expected");
		}
		catch ( QualityOutOfRangeException  e){
			assertThat(e)
					.isInstanceOf(QualityOutOfRangeException.class)
					.hasMessage(ERROR_MESSAGE);
		}

		try {
			ItemsHandlersFactory.createBackstagePassesHandler(ItemHandler.MAX_QUALITY + ONE, SELL_IN_FIFTHTEEN_DAYS);
			Assertions.fail("Error: QualityOutOfRangeException quality exception expected");
		}
		catch ( QualityOutOfRangeException  e){
			assertThat(e)
					.isInstanceOf(QualityOutOfRangeException.class)
					.hasMessage(ERROR_MESSAGE);
		}

		try {
			ItemsHandlersFactory.createConjuredItemHandler(SWORD, ItemHandler.MAX_QUALITY + ONE,SELL_IN_FIFTHTEEN_DAYS);
			Assertions.fail("Error: QualityOutOfRangeException quality exception expected");
		}
		catch ( QualityOutOfRangeException  e){
			assertThat(e)
					.isInstanceOf(QualityOutOfRangeException.class)
					.hasMessage(ERROR_MESSAGE);
		}
	}

	@Test
	public void testConjuredItemQualityDecreaseByTwoBeforeSellIn() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createConjuredItemHandler(SWORD, QUALITY_IS_TWENTY,SELL_IN_FIFTHTEEN_DAYS)
		};
		GildedRose app = new GildedRose(handlers);
		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY - TWO);
	}

	@Test
	public void testConjuredItemQualityDecreaseByFourAfterSellIn() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createConjuredItemHandler(SWORD, QUALITY_IS_TWENTY,A_DAY_AFTER_THE_SELL_IN) };
		GildedRose app = new GildedRose(handlers);


		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(QUALITY_IS_TWENTY - FOUR);
	}

	@Test
	public void testQualityNeverDropsLessthanZeroForConjuredItems() throws QualityOutOfRangeException{

		ItemHandler[] handlers = new ItemHandler[] {
				ItemsHandlersFactory.createConjuredItemHandler(SWORD, ItemHandler.MIN_QUALITY + ONE,SELL_IN_FIFTHTEEN_DAYS) };
		GildedRose app = new GildedRose(handlers);


		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(ConjuredItemHandler.MIN_QUALITY );
		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(ConjuredItemHandler.MIN_QUALITY);
		app.updateQuality();
		assertThat(app.getItem(FIRST_ITEM).quality).isEqualTo(ConjuredItemHandler.MIN_QUALITY);
	}















}
